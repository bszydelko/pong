module dynamicSprite
	#(	
		parameter R = 255,
		parameter G = 255,
		parameter B = 255
	)
	(
	input wire		   	CLK,
	input wire		   	RST,
	
	input wire [10:0] 	iH_CNT,
	input wire [10:0]  	iV_CNT,
	
	input wire [7:0]   	iRED,
	input wire [7:0]   	iGREEN,
	input wire [7:0]   	iBLUE,	 
	
	
	input wire [10:0]  	iW,
	input wire [10:0]  	iH,
	
	input wire [10:0]  	iX,
	input wire [10:0]  	iY,
	
	output reg [10:0]  	oH_CNT,
	output reg [10:0]  	oV_CNT,
	
	output reg [7:0] 	oRED,
	output reg [7:0]  	oGREEN,
	output reg [7:0]  	oBLUE
	);	
	
	always @(posedge CLK or posedge RST) 
			if(RST) begin
					oRED 	<= iRED;
					oGREEN 	<= iGREEN;
					oBLUE 	<= iBLUE;
				end
			
			else if(((iH_CNT >= iX) && (iH_CNT < (iX + iW))) && ((iV_CNT >= iY) && (iV_CNT < (iY + iH)))) begin
					oRED 	<= R;
					oGREEN 	<= G;
					oBLUE 	<= B;
			end else begin
					oRED 	<= iRED;
					oGREEN 	<= iGREEN;
					oBLUE 	<= iBLUE;
				end
	
	
	always @(posedge CLK) begin
			oH_CNT <= iH_CNT;
			oV_CNT <= iV_CNT;
		end
endmodule



