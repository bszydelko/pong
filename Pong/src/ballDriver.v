module ballDriver	
	#(
	parameter SPEED = 1,
	parameter X = 0,
	parameter Y = 0,
	parameter W = 0,
	parameter H = 0,
	
	// vertical boundaries
	parameter Y_top_limit = 0,
	parameter Y_bottom_limit = 0,
	
	parameter PAD_H = 0
	
	)
	(		   
	input wire 			CLK,
	input wire 			RST,	
	input wire 			TICK,
	
	input wire [10:0]  	iH_CNT,
	input wire [10:0]  	iV_CNT,	
	
	input wire [7:0]   	iRED,
	input wire [7:0]   	iGREEN,
	input wire [7:0]   	iBLUE,	 
	
	input wire [10:0]	iY_PAD_LEFT,
	input wire [10:0]	iY_PAD_RIGHT, 
	
	input wire [10:0] X_left_limit,
	input wire [10:0] X_right_limit,
	
	output reg [10:0] 	oH_CNT,
	output reg [10:0]   oV_CNT,
	
	output reg [7:0] 	oRED,
	output reg [7:0]  	oGREEN,
	output reg [7:0]  	oBLUE,
	
	output reg [10:0]	oX_POS,
	output reg [10:0]	oY_POS,
	
	output wire [3:0] oLEFT_SCORE,
	output wire [3:0] oRIGHT_SCORE
	
	);
	
	reg signed [10:0] X_pos;
	reg signed [10:0] Y_pos;
	reg signed [10:0] prev_X_pos;
	reg signed [10:0] prev_Y_pos;
	reg signed [10:0] dx;
	reg signed [10:0] dy;
	
	reg [2:0] left_score;
	reg [2:0] right_score;
	
	reg [31:0] delay;
	reg delay_wait;
	
	assign oLEFT_SCORE = left_score;
	assign oRIGHT_SCORE = right_score;
	
	always@(posedge CLK) begin
			oH_CNT <= iH_CNT;
			oV_CNT <= iV_CNT;
		end
	
	always@(posedge CLK) begin
			oRED 	<= iRED;
			oGREEN 	<= iGREEN;
			oBLUE 	<= iBLUE;	
		end		
	
	always @(posedge TICK or posedge RST) begin   
			if (RST) begin
					X_pos <= X;
					Y_pos <= Y;
					prev_X_pos <= X;
					prev_Y_pos <= Y;
					
					left_score <= 0;
					right_score <= 0;
					
					delay <= 0;
					delay_wait <= 0;
					
					dx <= 1;
					dy <= 1;
				end
			else if(TICK) begin	
					
					
					if(left_score == 7 || right_score == 7) begin
					
						delay_wait <= 1;
						delay <= 0;
						end
						
					if(delay[9] == 1) begin
						left_score <= 0;
						right_score <= 0;
						delay_wait <= 0;
						delay <= 0;
					end
					
						
					if(delay_wait == 1) begin
						delay <= 1 + delay;
						end
					
					if (delay_wait == 0) begin
						X_pos <= X_pos + (dx*SPEED);
						Y_pos <= Y_pos + (dy*SPEED);
					end
					
					if(Y_pos < Y_top_limit && delay_wait == 0) begin 	 
							Y_pos <= Y_top_limit;	
							X_pos <= (X_pos + prev_X_pos) / 2;
							dy <= -1*dy;
						end	  
					else if(Y_pos+H > Y_bottom_limit && delay_wait == 0) begin		 
							Y_pos <= Y_bottom_limit-H; 	
							X_pos <= (X_pos + prev_X_pos) / 2;
							dy <= -1*dy;
						end	 
					else if (X_pos < X_left_limit && delay_wait == 0)	begin 
							if((Y_pos > iY_PAD_LEFT) && (Y_pos <= (iY_PAD_LEFT+PAD_H))) begin	// left paddle bounce	   
									
									X_pos <= X_left_limit;	 
									Y_pos <= (Y_pos + prev_Y_pos) / 2;
									dx <= -1 * dx;
								end
							else begin
								right_score <= 1 + right_score;
								X_pos <= X;
								Y_pos <= Y;
								dx <= -1 * dx;
								
								end
						end	
					else if	(X_pos+W > X_right_limit &&  delay_wait == 0) begin	 
							if((Y_pos > iY_PAD_RIGHT) && (Y_pos <= (iY_PAD_RIGHT+PAD_H))) begin // right paddle bounce	 
									
									X_pos <= X_right_limit - W;	
									Y_pos <= (Y_pos + prev_Y_pos) / 2;
									dx <= -1 * dx;
								
								end
							else begin
								left_score <= 1 + left_score;  
								X_pos <= X;
								Y_pos <= Y;
								dx <= -1 * dx;
								end
						end	
				
					
					if(delay_wait == 0) begin
						prev_X_pos <= X_pos;
						prev_Y_pos <= Y_pos; 
					end 
						oX_POS <= X_pos;
						oY_POS <= Y_pos;
					
					
					
				end
		end	
endmodule