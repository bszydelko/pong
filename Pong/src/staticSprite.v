module staticSprite
	#(	
	// (X,Y) as top-left position
	parameter X = 0,
	parameter Y = 0,
	parameter W = 0,
	parameter H = 0,
	
	parameter R = 255,
	parameter G = 255,
	parameter B = 255
	)
	(
	input wire		   CLK,
	input wire		   RST,
	
	input wire [10:0]  iH_CNT,
	input wire [10:0]  iV_CNT,
	
	input wire [7:0]   iRED,
	input wire [7:0]   iGREEN,
	input wire [7:0]   iBLUE,	
	
	output reg [10:0]  oH_CNT,
	output reg [10:0]  oV_CNT,
	
	output reg [7:0]  oRED,
	output reg [7:0]  oGREEN,
	output reg [7:0]  oBLUE
	);

	always @(posedge CLK or posedge RST)
			if(RST) begin
					oRED 	<= iRED;
					oGREEN 	<= iGREEN;
					oBLUE 	<= iBLUE;
				end
			else if(((iH_CNT >= X) && (iH_CNT < (X + W))) && ((iV_CNT >= Y) && (iV_CNT < (Y + H)))) begin
					oRED 	<= R;
					oGREEN 	<= G;
					oBLUE 	<= B;
			end else begin
					oRED 	<= iRED;
					oGREEN 	<= iGREEN;
					oBLUE 	<= iBLUE;
				end
		
	
	always @(posedge CLK) begin
			oH_CNT <= iH_CNT;
			oV_CNT <= iV_CNT;
		end
endmodule



