module paddleDriver
	#(
	parameter SPEED = 1,
	parameter Y = 0,
	parameter H = 0,
	
	// vertical boundaries
	parameter Y_top_limit = 0,
	parameter Y_bottom_limit = 0
	)
	(		   
	input wire 			CLK,
	input wire 			RST,	
	input wire 			TICK,
	
	input wire [10:0]  	iH_CNT,
	input wire [10:0]  	iV_CNT,	

	input wire [7:0]   	iRED,
	input wire [7:0]   	iGREEN,
	input wire [7:0]   	iBLUE,
	
	input wire 			ENC_QA,
	input wire			ENC_QB,
	
	output reg [10:0] 	oH_CNT,
	output reg [10:0]  	oV_CNT,	

	output reg [7:0] 	oRED,
	output reg [7:0]  	oGREEN,
	output reg [7:0]  	oBLUE,							   
															   
	output reg [10:0]	oY_POS
	);
	
	reg ENC_QA_DEL;
	reg ENC_QB_DEL;
	
	always @(posedge CLK) begin
			ENC_QA_DEL <= ENC_QA;	  
			ENC_QB_DEL <= ENC_QB;  
		end	
	
	wire ENC_ENABLE = ENC_QA ^ ENC_QA_DEL ^ ENC_QB ^ ENC_QB_DEL;
	wire ENC_DIRECTION = ENC_QA ^ ENC_QB_DEL;

	reg signed [10:0] Y_pos;
	
	always @(posedge CLK)begin
		oH_CNT 	<= iH_CNT;
		oV_CNT 	<= iV_CNT;
	end

	always@(posedge CLK) begin
		oRED 	<= iRED;
		oGREEN 	<= iGREEN;
		oBLUE 	<= iBLUE;
	end
		
	always @(posedge CLK or posedge RST) begin  
		if(RST) begin
			Y_pos <= Y;
		end	
		else if(TICK) begin
			if (Y_pos < Y_top_limit) 	Y_pos <= Y_top_limit;
			if (Y_pos + H > Y_bottom_limit) Y_pos <= Y_bottom_limit - H; // clamp paddle
			oY_POS <= Y_pos;
		end
				
		else if(ENC_ENABLE) begin
					if(ENC_DIRECTION) 	Y_pos <= Y_pos + SPEED;
					else begin
						if((Y_pos - SPEED) < 0) Y_pos <= Y_top_limit;
						else Y_pos <= Y_pos - SPEED;
						end
						
				end
		end
endmodule
