module encoderFilter
	#(
	parameter valid0 = 4'b0000,
	parameter valid1 = 4'b1111
	)
	(
	input wire CLK,
	input wire RST,
	
	input wire IN,
	output reg OUT
	
	);
	reg [3:0] dfilter4;
	
	always @(posedge CLK or posedge RST) begin
		if (RST)
			dfilter4 <= 4'b0000;
		else begin
			dfilter4 <= {dfilter4[2:0],IN};
			case(dfilter4)
				valid0: OUT <= 0;
				valid1: OUT <= 1;
				default: OUT <= OUT;
			endcase
			end

		end
		
	
endmodule