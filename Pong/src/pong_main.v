module pong_main
	#(
	parameter SCR_W = 30,
	parameter SCR_H = 20
	
	)
	(
	input wire        CLK, // CLK 75MHz
	input wire        RST, // Active high reset
	
	input wire [10:0] H_CNT, // horizontal pixel pointer
	input wire [10:0] V_CNT, // vertical   pixel pointer
	
	input wire        EncA_QA, 
	input wire        EncA_QB,
	input wire        EncB_QA,
	input wire        EncB_QB,
	
	output wire [7:0] RED,
	output wire [7:0] GREEN,
	output wire [7:0] BLUE,
	
	output wire [3:0] LED
	);
	
	
	
	//localparam unitSize = (SCR_W);
	localparam unitSize_param = 10;
	wire [10:0] unitSize = 10;
	localparam PAD_H_param = 160;
	wire [10:0] PAD_H = 160; 
	//localparam PAD_H = 6;
	//localparam PAD_H = 80;
	
	// Constant output 
	//assign RED   = 8'hFF;
	//assign GREEN = 8'hFF;
	//assign BLUE  = 8'hFF;
	wire [7:0] DEF_RED = 0;
	wire [7:0] DEF_GREEN = 0;
	wire [7:0] DEF_BLUE = 0;
	
	
	
	//-----------------------------------------
	// assign LED to counter bits to indicate FPGA is working
	reg [31:0] heartbeat;
	always@(posedge CLK or posedge RST)
		if(RST) heartbeat <=             32'd0;
		else    heartbeat <= heartbeat + 32'd1;	   
	 
	wire tick = &heartbeat[20:15];	

	//wire tick = heartbeat[9];
	
	wire filtered_EncA_QA;
	wire filtered_EncA_QB;
	wire filtered_EncB_QA;
	wire filtered_EncB_QB;
	
	encoderFilter fiter_EncA_QA (
		.CLK(CLK),
		.RST(RST),
		.IN(EncA_QA),
		.OUT(filtered_EncA_QA)
	);
	
	encoderFilter fiter_EncA_QB (
		.CLK(CLK),
		.RST(RST),
		.IN(EncA_QB),
		.OUT(filtered_EncA_QB)
	);
	
	encoderFilter fiter_EncB_QA (
		.CLK(CLK),
		.RST(RST),
		.IN(EncB_QA),
		.OUT(filtered_EncB_QA)
	);
	
	encoderFilter fiter_EncB_QB (
		.CLK(CLK),
		.RST(RST),
		.IN(EncB_QB),
		.OUT(filtered_EncB_QB)
	);
	
	wire [7:0] 	R_border_RED;
	wire [7:0] 	R_border_GREEN;
	wire [7:0] 	R_border_BLUE;
	wire [10:0] 	R_border_H_CNT;
	wire [10:0] 	R_border_V_CNT;

	staticSprite #( .X(0), .Y(0), .W(SCR_W), .H(unitSize_param))
		T_border (
			.CLK	(CLK			),
			.RST	(RST			),
			.iH_CNT	(H_CNT			),
			.iV_CNT	(V_CNT			),
			.iRED	(DEF_RED			),
			.iGREEN	(DEF_GREEN			),
			.iBLUE	(DEF_BLUE			),
			
			.oH_CNT	(R_border_H_CNT	),
			.oV_CNT	(R_border_V_CNT	),		
			.oRED	(R_border_RED	),
			.oGREEN	(R_border_GREEN	),
			.oBLUE	(R_border_BLUE	)
		);
	
	wire [7:0] 	B_border_RED;
	wire [7:0] 	B_border_GREEN;
	wire [7:0] 	B_border_BLUE;
	wire [10:0] 	B_border_H_CNT;
	wire [10:0] 	B_border_V_CNT;

	staticSprite #( .X(SCR_W-unitSize_param), .Y(0), .W(unitSize_param), .H(SCR_H))
		R_border (
			.CLK	(CLK			),
			.RST	(RST			),
			.iH_CNT	(R_border_H_CNT	),
			.iV_CNT	(R_border_V_CNT	),
			.iRED	(R_border_RED	),
			.iGREEN	(R_border_GREEN	),
			.iBLUE	(R_border_BLUE	),

			.oH_CNT	(B_border_H_CNT	),
			.oV_CNT	(B_border_V_CNT	),
			.oRED	(B_border_RED	),
			.oGREEN	(B_border_GREEN	),
			.oBLUE	(B_border_BLUE	)
		);

	wire [7:0] 	L_border_RED;
	wire [7:0] 	L_border_GREEN;
	wire [7:0] 	L_border_BLUE;
	wire [10:0] 	L_border_H_CNT;
	wire [10:0] 	L_border_V_CNT;  

	staticSprite #( .X(0), .Y(SCR_H-unitSize_param), .W(SCR_W), .H(unitSize_param))
		B_border (
			.CLK	(CLK			),
			.RST	(RST			),
			.iH_CNT	(B_border_H_CNT	),
			.iV_CNT	(B_border_V_CNT	),
			.iRED	(B_border_RED	),
			.iGREEN	(B_border_GREEN	),
			.iBLUE	(B_border_BLUE	),

			.oH_CNT	(L_border_H_CNT	),
			.oV_CNT	(L_border_V_CNT	),
			.oRED	(L_border_RED	),
			.oGREEN	(L_border_GREEN	),
			.oBLUE	(L_border_BLUE	)
		);
	
	wire [7:0] 	L_paddle_driver_RED;
	wire [7:0] 	L_paddle_driver_GREEN;
	wire [7:0] 	L_paddle_driver_BLUE;
	wire [10:0] 	L_paddle_driver_H_CNT;
	wire [10:0] 	L_paddle_driver_V_CNT;  

	staticSprite #( .X(0), .Y(0), .W(unitSize_param), .H(SCR_H))
		L_border (
			.CLK	(CLK					),
			.RST	(RST					),
			.iH_CNT	(L_border_H_CNT			),
			.iV_CNT	(L_border_V_CNT			),
			.iRED	(L_border_RED			),
			.iGREEN	(L_border_GREEN			),
			.iBLUE	(L_border_BLUE			),

			.oH_CNT	(L_paddle_driver_H_CNT	),
			.oV_CNT	(L_paddle_driver_V_CNT	),
			.oRED	(L_paddle_driver_RED	),
			.oGREEN	(L_paddle_driver_GREEN	),
			.oBLUE	(L_paddle_driver_BLUE	)
		); 
	
	
	
	// dynamic sprites
	wire [7:0] 	L_paddle_RED;
	wire [7:0] 	L_paddle_GREEN;
	wire [7:0] 	L_paddle_BLUE;
	wire [10:0] L_paddle_X = L_border.X + L_border.W + unitSize+1;
	wire [10:0] L_paddle_Y;
	wire [10:0] L_paddle_H_CNT;
	wire [10:0] L_paddle_V_CNT; 
	localparam PAD_SPEED = 20;

	paddleDriver # ( .SPEED(PAD_SPEED), .Y((SCR_H/2)-(PAD_H_param/2)), .H(PAD_H_param), .Y_top_limit(unitSize_param), .Y_bottom_limit(SCR_H-unitSize_param))
		L_paddle_driver (	
			.CLK	(CLK					),
			.RST	(RST					),
			.TICK	(tick					),
			.iH_CNT	(L_border_H_CNT			),
			.iV_CNT	(L_border_V_CNT			),
			.iRED	(L_paddle_driver_RED	),
			.iGREEN	(L_paddle_driver_GREEN	),
			.iBLUE	(L_paddle_driver_BLUE	),
			.ENC_QA	(filtered_EncA_QA				),
			.ENC_QB	(filtered_EncA_QB				),

			.oH_CNT	(L_paddle_H_CNT			),
			.oV_CNT	(L_paddle_V_CNT			),
			.oRED	(L_paddle_RED			),
			.oGREEN	(L_paddle_GREEN			),
			.oBLUE	(L_paddle_BLUE			),
			.oY_POS	(L_paddle_Y				)
		);
	
	wire [7:0] 	R_paddle_driver_RED;	
	wire [7:0] 	R_paddle_driver_GREEN;
	wire [7:0] 	R_paddle_driver_BLUE;
	wire [10:0] R_paddle_driver_H_CNT;
	wire [10:0] R_paddle_driver_V_CNT;  

	dynamicSprite # (.R(255), .G(255), .B(255))
		L_paddle (
			.CLK	(CLK					),
			.RST	(RST					),
			.iH_CNT	(L_paddle_H_CNT			),
			.iV_CNT	(L_paddle_V_CNT			),
			.iRED	(L_paddle_RED			),
			.iGREEN	(L_paddle_GREEN			),
			.iBLUE	(L_paddle_BLUE			),
			.iW 	(unitSize),
			.iH		(PAD_H),
			.iX		(L_paddle_X				),
			.iY		(L_paddle_Y				),

			.oH_CNT	(R_paddle_driver_H_CNT	),
			.oV_CNT	(R_paddle_driver_V_CNT	),
			.oRED	(R_paddle_driver_RED	),
			.oGREEN	(R_paddle_driver_GREEN	),
			.oBLUE	(R_paddle_driver_BLUE	)
		);

	wire [7:0] 	R_paddle_RED;
	wire [7:0] 	R_paddle_GREEN;
	wire [7:0] 	R_paddle_BLUE;
	wire [10:0] R_paddle_X = R_border.X - 2*unitSize;
	wire [10:0] R_paddle_Y;
	wire [10:0] R_paddle_H_CNT;
	wire [10:0] R_paddle_V_CNT;  

	paddleDriver # ( .SPEED(PAD_SPEED), .Y((SCR_H/2)-(PAD_H_param/2)), .H(PAD_H_param), .Y_top_limit(unitSize_param), .Y_bottom_limit(SCR_H-unitSize_param))
		R_paddle_driver (	
			.CLK	(CLK					),
			.RST	(RST					),
			.TICK	(tick					),
			.iH_CNT	(R_paddle_driver_H_CNT	),
			.iV_CNT	(R_paddle_driver_V_CNT	),
			.iRED	(R_paddle_driver_RED	),
			.iGREEN	(R_paddle_driver_GREEN	),
			.iBLUE	(R_paddle_driver_BLUE	),
			.ENC_QA	(filtered_EncB_QA				),
			.ENC_QB	(filtered_EncB_QB				),

			.oH_CNT	(R_paddle_H_CNT			),
			.oV_CNT	(R_paddle_V_CNT			),
			.oRED	(R_paddle_RED			),
			.oGREEN	(R_paddle_GREEN			),
			.oBLUE	(R_paddle_BLUE			),
			.oY_POS	(R_paddle_Y				)
		);

	wire [7:0] 	ball_driver_RED;	
	wire [7:0] 	ball_driver_GREEN;
	wire [7:0] 	ball_driver_BLUE;
	wire [10:0] ball_driver_H_CNT;
	wire [10:0] ball_driver_V_CNT; 

	dynamicSprite #(.R(255), .G(255), .B(255))
		R_paddle (
			.CLK	(CLK				),
			.RST	(RST				),
			.iH_CNT	(R_paddle_H_CNT		),
			.iV_CNT	(R_paddle_V_CNT		),
			.iRED	(R_paddle_RED		),
			.iGREEN	(R_paddle_GREEN		),
			.iBLUE	(R_paddle_BLUE		),
			.iW		(unitSize),
			.iH		(PAD_H),
			.iX		(R_paddle_X			),
			.iY		(R_paddle_Y			),

			.oH_CNT	(ball_driver_H_CNT	),
			.oV_CNT	(ball_driver_V_CNT	),
			.oRED	(ball_driver_RED	),
			.oGREEN	(ball_driver_GREEN	),
			.oBLUE	(ball_driver_BLUE	)
		);

	wire [7:0] 	ball_RED;	
	wire [7:0] 	ball_GREEN;
	wire [7:0] 	ball_BLUE;
	wire [10:0] ball_H_CNT;
	wire [10:0] ball_V_CNT; 
	wire [10:0] ball_X;
	wire [10:0] ball_Y; 
	
	wire [3:0] left_score;
	wire [3:0] right_score;

	ballDriver #( .SPEED(7), .X(SCR_W/2), .Y(SCR_H/2), .W(unitSize_param), .H(unitSize_param), .Y_top_limit(unitSize_param), .Y_bottom_limit(SCR_H-unitSize_param), .PAD_H(PAD_H_param))
		ball_driver(
			.CLK			(CLK				),
			.RST			(RST				),	
			.TICK			(tick				),
	  		.iH_CNT			(ball_driver_H_CNT	),
	  		.iV_CNT			(ball_driver_V_CNT	),	
	  		.iRED			(ball_driver_RED	),
	  		.iGREEN			(ball_driver_GREEN	),
	  		.iBLUE			(ball_driver_BLUE	),	 
			.iY_PAD_LEFT	(L_paddle_Y			),
			.iY_PAD_RIGHT	(R_paddle_Y			),
		   	.X_left_limit 	(L_paddle_X+unitSize),
			.X_right_limit  (R_paddle_X			),
			
			
	 		.oH_CNT			(ball_H_CNT			),
	  		.oV_CNT			(ball_V_CNT			),
			.oRED			(ball_RED			),
	 		.oGREEN			(ball_GREEN			),
	 		.oBLUE			(ball_BLUE			),
			.oX_POS			(ball_X				),
			.oY_POS			(ball_Y				),
			.oLEFT_SCORE	(left_score),
			.oRIGHT_SCORE	(right_score)
		);

	wire [7:0] 	M_line_RED;
	wire [7:0] 	M_line_GREEN;
	wire [7:0] 	M_line_BLUE;
	wire [10:0] M_line_H_CNT;
	wire [10:0] M_line_V_CNT;
	
	dynamicSprite #(.R(255), .G(255), .B(255))
		ball (
			.CLK	(CLK		),
			.RST	(RST		),
			.iH_CNT	(ball_H_CNT	),
			.iV_CNT	(ball_V_CNT	),
			.iRED	(ball_RED	),
			.iGREEN	(ball_GREEN	),
			.iBLUE	(ball_BLUE	),
			.iW		(unitSize),
			.iH 	(unitSize),
			.iX		(ball_X		),
			.iY		(ball_Y		),
			.oH_CNT	(M_line_H_CNT			),
			.oV_CNT	(M_line_V_CNT			),
			.oRED	(M_line_RED		),
			.oGREEN	(M_line_GREEN		),
			.oBLUE	(M_line_BLUE		)
		);
		
		
	wire [7:0] 	LEFT_score_bground_RED;
	wire [7:0] 	LEFT_score_bground_GREEN;
	wire [7:0] 	LEFT_score_bground_BLUE;
	wire [10:0] 	LEFT_score_bground_H_CNT;
	wire [10:0] 	LEFT_score_bground_V_CNT;
	
	staticSprite #( .X(SCR_W/2), .Y(0), .W(unitSize_param/2), .H(SCR_H))
		M_line (
			.CLK	(CLK			),
			.RST	(RST			),
			.iH_CNT	(M_line_H_CNT	),
			.iV_CNT	(M_line_V_CNT	),
			.iRED	(M_line_RED	),
			.iGREEN	(M_line_GREEN	),
			.iBLUE	(M_line_BLUE	),

			.oH_CNT	(LEFT_score_bground_H_CNT),
			.oV_CNT	(LEFT_score_bground_V_CNT),
			.oRED	(LEFT_score_bground_RED	),
			.oGREEN	(LEFT_score_bground_GREEN	),
			.oBLUE	(LEFT_score_bground_BLUE	)
		);

	wire [7:0] 	LEFT_score_RED;
	wire [7:0] 	LEFT_score_GREEN;
	wire [7:0] 	LEFT_score_BLUE;
	wire [10:0] 	LEFT_score_H_CNT;
	wire [10:0] 	LEFT_score_V_CNT;

	localparam score_max = 7;
	staticSprite #( .X(SCR_W/4 - ((unitSize_param * score_max)/2)), .Y(2 * unitSize_param), .W(unitSize_param * score_max), .H(unitSize_param), .R(0), .G(102), .B(0))
		LEFT_score_bground (
			.CLK	(CLK			),
			.RST	(RST			),
			.iH_CNT	(LEFT_score_bground_H_CNT	),
			.iV_CNT	(LEFT_score_bground_V_CNT	),
			.iRED	(LEFT_score_bground_RED	),
			.iGREEN	(LEFT_score_bground_GREEN	),
			.iBLUE	(LEFT_score_bground_BLUE	),

			.oH_CNT	(LEFT_score_H_CNT),
			.oV_CNT	(LEFT_score_V_CNT),
			.oRED	(LEFT_score_RED	),
			.oGREEN	(LEFT_score_GREEN	),
			.oBLUE	(LEFT_score_BLUE	)
		);
		
		
	
	
	reg [10:0] LEFT_score_X = (SCR_W / 4) - (unitSize_param * score_max) / 2;
	reg [10:0] LEFT_score_Y = unitSize_param * 2;
	
	wire [7:0] 	RIGHT_score_bground_RED;
	wire [7:0] 	RIGHT_score_bground_GREEN;
	wire [7:0] 	RIGHT_score_bground_BLUE;
	wire [10:0] 	RIGHT_score_bground_H_CNT;
	wire [10:0] 	RIGHT_score_bground_V_CNT;
	
	dynamicSprite # (.R(127), .G(255), .B(127) )
		LEFT_score (
			.CLK	(CLK					),
			.RST	(RST					),
			.iH_CNT	(LEFT_score_H_CNT			),
			.iV_CNT	(LEFT_score_V_CNT		),
			.iRED	(LEFT_score_RED		),
			.iGREEN	(LEFT_score_GREEN	),
			.iBLUE	(LEFT_score_BLUE		),
			.iW 	(left_score * unitSize_param),
			.iH		(unitSize),
			.iX		(LEFT_score_X		),
			.iY		(LEFT_score_Y		),

			.oH_CNT	(RIGHT_score_bground_H_CNT),
			.oV_CNT	(RIGHT_score_bground_V_CNT),
			.oRED	(RIGHT_score_bground_RED	),
			.oGREEN	(RIGHT_score_bground_GREEN	),
			.oBLUE	(RIGHT_score_bground_BLUE	)
		);
		
		//right
		
	wire [7:0] 	RIGHT_score_RED;
	wire [7:0] 	RIGHT_score_GREEN;
	wire [7:0] 	RIGHT_score_BLUE;
	wire [10:0] 	RIGHT_score_H_CNT;
	wire [10:0] 	RIGHT_score_V_CNT;

	
	staticSprite #( .X((SCR_W/4 )*3 - ((unitSize_param * score_max)/2)), .Y(2 * unitSize_param), .W(unitSize_param * score_max), .H(unitSize_param), .R(0), .G(102), .B(0))
		RIGHT_score_bground (
			.CLK	(CLK			),
			.RST	(RST			),
			.iH_CNT	(RIGHT_score_bground_H_CNT	),
			.iV_CNT	(RIGHT_score_bground_V_CNT	),
			.iRED	(RIGHT_score_bground_RED	),
			.iGREEN	(RIGHT_score_bground_GREEN	),
			.iBLUE	(RIGHT_score_bground_BLUE	),

			.oH_CNT	(RIGHT_score_H_CNT),
			.oV_CNT	(RIGHT_score_V_CNT),
			.oRED	(RIGHT_score_RED	),
			.oGREEN	(RIGHT_score_GREEN	),
			.oBLUE	(RIGHT_score_BLUE	)
		);
		
	reg [10:0] RIGHT_score_X = ((SCR_W / 4)* 3) - (unitSize_param * score_max) / 2;
	reg [10:0] RIGHT_score_Y = unitSize_param * 2;
	dynamicSprite # (.R(127), .G(255), .B(127) )
		RIGHT_score (
			.CLK	(CLK					),
			.RST	(RST					),
			.iH_CNT	(RIGHT_score_H_CNT			),
			.iV_CNT	(RIGHT_score_V_CNT		),
			.iRED	(RIGHT_score_RED		),
			.iGREEN	(RIGHT_score_GREEN	),
			.iBLUE	(RIGHT_score_BLUE		),
			.iW 	(right_score * unitSize_param),
			.iH		(unitSize),
			.iX		(RIGHT_score_X		),
			.iY		(RIGHT_score_Y		),

			.oH_CNT	(	),
			.oV_CNT	(	),
			.oRED	(RED	),
			.oGREEN	(GREEN	),
			.oBLUE	(BLUE	)
		);
	
	assign LED[3:0] = heartbeat[26:23];
	
	//-----------------------------------------
endmodule